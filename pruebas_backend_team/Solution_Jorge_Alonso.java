package com.example.pruebas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution_Jorge_Alonso {

    public int[] solution(Integer K, Integer M, int[] initialArray) {

        if (!checkSegmentLength(initialArray, K) && !checkBiggerValue(M, initialArray)) {

            List<List<Integer>> listsModified = getAllListsModified(initialArray, K);

            Set<Integer> leadersSet = new HashSet<>();

            listsModified.parallelStream().forEach(c -> leadersSet.add(getListLeader(c)));

            return leadersSet.stream().filter(c -> c > 0).distinct().mapToInt(Integer::intValue).toArray();

        } else {
            return new int[]{};
        }
    }

    private boolean checkSegmentLength(int[] initialArray, Integer K) {
        return K > initialArray.length;
    }

    private boolean checkBiggerValue(Integer M, int[] initialArray) {
        return Arrays.stream(initialArray).filter(c -> c > M).count() != 0;
    }

    private List<List<Integer>> getAllListsModified(int[] arrayA, int segmentLength) {
        List<List<Integer>> allListModified = new ArrayList<>();

        List<Integer> initialList = Arrays.stream(arrayA).boxed().collect(Collectors.toList());

        int initialPos = 0;
        int finalPos = segmentLength - 1;

        while (finalPos <= initialList.size()) {

            allListModified.add(modifiedSegmentList(initialList, initialPos, finalPos));

            initialPos = initialPos + 1;
            finalPos = finalPos + 1;

        }
        return allListModified;

    }

    private List<Integer> modifiedSegmentList(List<Integer> listToModify, Integer initialPos, Integer finalPos) {

        List<Integer> modifiedList = new ArrayList<>();

        IntStream.range(0, listToModify.size())
                .forEach(i -> {
                    if (i >= initialPos && i <= finalPos) {
                        modifiedList.add(listToModify.get(i) + 1);
                    } else {
                        modifiedList.add(listToModify.get(i));
                    }
                });

        return modifiedList;
    }

    private Integer getListLeader(List<Integer> integersList) {

        Map<Integer, Long> mapLeadersWithOccurrences;

        mapLeadersWithOccurrences = integersList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Map.Entry<Integer, Long> entryLeader = mapLeadersWithOccurrences.entrySet().stream().max(Map.Entry.comparingByValue()).orElse(null);

        if (entryLeader != null) {
            int leader = entryLeader.getKey();
            int leaderOccurrences = entryLeader.getValue().intValue();

            if (isLeader(integersList, leaderOccurrences)) {
                return leader;
            }

        }
        return 0;
    }

    private boolean isLeader(List<Integer> integerList, int leaderOccurrences) {
        return leaderOccurrences > (integerList.size() / 2);
    }

}
