package com.ing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/* Jesús Lorenzo Martín de Dios */
public class Solution {

	private final int BOOSTER = 1;
	private final int MAX_INT_VALUE_ADMITED = 100000;

	public int[] solution(int k_range, int m_maxValue, int[] a_originalList) {

		if(isNotRangeGreaterThanListSize(k_range, a_originalList) && checkMaxValueAdmited(k_range, m_maxValue)) {

			List<Integer> potencialLeaders = Arrays.stream(a_originalList).boxed().collect(Collectors.toList());

			if(isPosibleOperate(potencialLeaders, m_maxValue)) {

				int initPosition = 0;
				int finalPosition = (initPosition + k_range - 1);

				List<Integer> leaders = processSegments(potencialLeaders, initPosition, finalPosition, k_range);
				return filterResult(leaders);
			}
		}
		return new int[0];
	}


	private boolean isNotRangeGreaterThanListSize(int k_range,  int[] a_originalList) {
		return k_range < a_originalList.length;
	}

	private boolean checkMaxValueAdmited(int k_range, int m_maxValue) {
		return MAX_INT_VALUE_ADMITED >= k_range && MAX_INT_VALUE_ADMITED >= m_maxValue;
	}

	private boolean isPosibleOperate(List<Integer> potencialLeaders, int m_maxValue) {
		return potencialLeaders.stream().anyMatch(num -> num < m_maxValue);
	}

	private List<Integer> processSegments(List<Integer> potencialLeaders, int initPosition, int finalPosition, int k_range) {

		List<Integer> leaders = new ArrayList<>();

		while(isPosibleSegment(potencialLeaders, finalPosition)) {

			List<Integer> leadersSegmentIncremeted = aplySegmentIncrement(potencialLeaders, initPosition, finalPosition);
			List<List<Integer>> leadersGrouped = groupLeaders(leadersSegmentIncremeted);

			setSegmentLeaders(potencialLeaders, leaders, leadersGrouped);

			initPosition ++;
			finalPosition = (initPosition + k_range -1);
		}
		return leaders;
	}

	private boolean isPosibleSegment(List<Integer> potencialLeaders, int finalPosition) {
		return finalPosition < potencialLeaders.size();
	}

	private List<Integer> aplySegmentIncrement(List<Integer> potencialLeaders, int initPosition, int finalPosition){

		List<Integer> leadersSegmentIncremeted = new ArrayList<>();

		IntStream.range(0, potencialLeaders.size()).forEach(index -> {

			if(index >= initPosition && index <= finalPosition) {
				leadersSegmentIncremeted.add(Integer.sum(potencialLeaders.get(index), BOOSTER));
			}else {
				leadersSegmentIncremeted.add(potencialLeaders.get(index));
			}
		});

		return leadersSegmentIncremeted;
	}

	private List<List<Integer>> groupLeaders(List<Integer> leadersSegmentIncremeted) {

		List<List<Integer>> leadersGrouped = leadersSegmentIncremeted.stream()
				.collect(Collectors.groupingBy(potencialLead -> potencialLead))
				.values().stream()
				.collect(Collectors.toList());
		return leadersGrouped;
	}

	private void setSegmentLeaders(List<Integer> potencialLeaders, List<Integer> leaders, List<List<Integer>> leadersGrouped) {

		for(List<Integer> leaderGrouped : leadersGrouped) {
			if(leaderGrouped.size() > (potencialLeaders.size()/2)) {
				leaders.add(leaderGrouped.get(0));
				break;
			}
		}
	}

	private int[] filterResult(List<Integer> leaders) {

		leaders.sort(Comparator.naturalOrder());

		return leaders.stream().distinct().collect(Collectors.toList())
				.stream().mapToInt(i -> i).toArray();
	}
}
